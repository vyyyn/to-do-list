import "./App.css";
import { Box, Checkbox, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";

function App() {
  const [list, setList] = useState();
  useEffect(() => {
    const getList = async() => {
      const url = 'https://jsonplaceholder.typicode.com/todos';
        await fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
              const listToDo = data?.slice(0, 10)
              if(listToDo){
                setList(listToDo)
              }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };
    getList();
  }, []);

  return (
    <Paper sx={{ width: "600px", m: "100px auto", p: "24px" }}>
      <h3>To do list</h3>
      <Box>
        {list?.map(v => {
          return <Typography mb="12px" key={v?.id}><Checkbox />{v?.title ?? "-"}</Typography>
        })}
        
      </Box>
    </Paper>
  );
}

export default App;
